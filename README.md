# On Tap

A quick-and-dirty digital display to showcase the beers on tap in my kegerator.

## TODO

### Project Stuff
- [ ] S3 upload script
- [ ] web structure: split CSS, JS, JSON
- [ ] JSX or some other template
- [ ] dynamic data upload

### Style
- [ ] force footer to bottom
- [ ] color theme

### Features
- [ ] archive/active? "on tap?"
- [ ] right/left tap control
- [ ] different glass icons
- [ ] ABV? color? IBU?

### Hardware
- [ ] mount and power
